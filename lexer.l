%{
#include <cstring>
#include "gram.tab.h"
#include "Token.hpp"
%}

%option yylineno

/* Define a state to ignore the commentaries: */
%s COMMENTARY

pidentifier [_a-z]+
num [0-9]+

%%
<INITIAL>
{
{num}           { Token t; t.line_no = yylineno; t.data.ival = atoll(yytext); yylval.token = t; return NUM; }
{pidentifier}   { Token t; t.line_no = yylineno; t.data.sval = strdup(yytext); yylval.token = t; return PIDENTIFIER; }

BEGIN		    { Token t; t.line_no = yylineno; yylval.token = t; return BEGIN_; }
DECLARE		    { Token t; t.line_no = yylineno; yylval.token = t; return DECLARE; }
DO		        { Token t; t.line_no = yylineno; yylval.token = t; return DO; }
DOWNTO		    { Token t; t.line_no = yylineno; yylval.token = t; return DOWNTO; }
ELSE		    { Token t; t.line_no = yylineno; yylval.token = t; return ELSE; }
END		        { Token t; t.line_no = yylineno; yylval.token = t; return END; }
ENDFOR		    { Token t; t.line_no = yylineno; yylval.token = t; return ENDFOR; }
ENDIF		    { Token t; t.line_no = yylineno; yylval.token = t; return ENDIF; }
ENDWHILE	    { Token t; t.line_no = yylineno; yylval.token = t; return ENDWHILE; }
FOR		        { Token t; t.line_no = yylineno; yylval.token = t; return FOR; }
FROM		    { Token t; t.line_no = yylineno; yylval.token = t; return FROM; }
IF		        { Token t; t.line_no = yylineno; yylval.token = t; return IF; }
READ		    { Token t; t.line_no = yylineno; yylval.token = t; return READ; }
REPEAT		    { Token t; t.line_no = yylineno; yylval.token = t; return REPEAT; }
THEN		    { Token t; t.line_no = yylineno; yylval.token = t; return THEN; }
TO		        { Token t; t.line_no = yylineno; yylval.token = t; return TO; }
UNTIL		    { Token t; t.line_no = yylineno; yylval.token = t; return UNTIL; }
WHILE		    { Token t; t.line_no = yylineno; yylval.token = t; return WHILE; }
WRITE		    { Token t; t.line_no = yylineno; yylval.token = t; return WRITE; }

\[              { BEGIN(COMMENTARY); }

[ \t\n]*        ;

.   		    { Token t; t.line_no = yylineno; yylval.token = t; return yytext[0]; }
}

<COMMENTARY>
{
\]              BEGIN(INITIAL);
.|\n            ;
}
%%
