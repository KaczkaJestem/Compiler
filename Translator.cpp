#include "Translator.hpp"

Translator::Translator(StatisticsContext& stat_context, bool debug_log)
    : memory_mgr(debug_log), statistics(stat_context)
{
    _program_tree = nullptr;
    _debug_log = debug_log;
}

Translator::~Translator()
{
    cleanup();
}

bool Translator::saveToFile(const char* filename) const
{
    _log("Saving assembly to: " + string(filename));

    ofstream out_file(filename, ios::trunc);

    if (!out_file.good())
    {
        _log("Failed to create file.");
        cerr << "Failed to create file: " << filename << endl;
        return false;
    }

    out_file << _assembly_code;

    _log("Saved.");
    out_file.close();
    
    return true;
}

void Translator::declareInt(const char* identifier, ull line_no)
{
    memory_mgr.declareInt(identifier, line_no);
}

void Translator::declareArray(const char* identifier, ull lower_bound, ull upper_bound, ull line_no)
{
    memory_mgr.declareArray(identifier, lower_bound, upper_bound, line_no);
}

void Translator::translate(vector<Command*>* program)
{
    _program_tree = program;

    _log("Program contains " + to_string(statistics.commands_count) + " commands.");
    
    if(_debug_log)
    {
        memory_mgr._printSymbolTable();
        _printSyntaxTree();
    }

    _log("Beginning translation");

    ull length = 0;
    _assembly_code.clear();

    cout << "Generating assembly code..." << endl;

    if(!_debug_log)
        statistics.monitorParsedCmdsPrecentage();

    for(vector<Command*>::iterator it = program->begin(); it != program->end(); ++it)
    {
        _assembly_code += (*it)->getCode(memory_mgr, length);
    }

    if(!_debug_log)
        statistics.stopMonitoring();

    _assembly_code += "HALT\n";

    cout << "Done." << endl;
    _log("Translation ended");
}

void Translator::cleanup()
{
    _assembly_code.clear();

    _log("Clearing Syntax Tree");
    if(_program_tree)
    {
        for(std::vector<Command*>::iterator it = _program_tree->begin();
            it != _program_tree->end(); ++it)
            delete *it;

        delete _program_tree;
        _program_tree = nullptr;
    }

    _log("Done.");
}

void Translator::clear_all()
{
    _log("Performing cleanup");
    cleanup();
    memory_mgr.cleanup();
    _log("Cleanup done.");
}

void Translator::_printSyntaxTree() const
{
    if(!_program_tree)
    {
        cout << "(&) Syntax Tree is uninitialized!" << endl;
        return;
    }

    cout << "(&) Syntax Tree content:" << endl;
    cout << "(&) ---------------------" << endl;

    for(std::vector<Command*>::const_iterator it = _program_tree->begin();
            it != _program_tree->end(); ++it)
    {
        (*it)->debugPrint();
    }

    cout << "(&) ---------------------" << endl;
}

void Translator::_log(std::string msg) const
{
    if(_debug_log)
        cout << "(+) " << msg << endl;
}