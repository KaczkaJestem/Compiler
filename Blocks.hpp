#ifndef __BLOCK_HPP__
#define __BLOCK_HPP__

#include <iostream>
#include <string>
#include <vector>
#include <cstdint>
#include <cmath>

#include "Symbol.hpp"
#include "StatisticsContext.hpp"
#include "MemoryManager.hpp"

/** A Block interface */
struct Block
{
    /** Index of input line in which this block occurs */
    ull line_no;
    /** Vector of clobber registers */
    vector<Register*> clobber_reg;

    /** Returns representational name of this block */
    virtual std::string getName() const = 0;
    /** Returns number of standalone Commands in this block */
    virtual ull getSize() const;
    /** Returns count of needed clober registers */
    virtual uint8_t getClobberCount() const = 0;

    void assignClobbers(MemoryManager& mgr);
    void freeClobbers(MemoryManager& mgr);

    /** Returns a string containing assembly code
    *  that implements this block's functionality.
    *  Offset parameter should be a line index of
    *  this fragment in the entire code.
    *  Length parameter is a reference to a variable
    *  that will contain the length of the generated
    *  assembly code.
    */
    virtual std::string getCode(MemoryManager& mgr, ull& length) = 0;
};

/** Reads a value stored in a variable or a constant. */
struct Value : public Block
{
    enum TYPE {
        T_CONSTANT,
        T_VARIABLE
    } type;

    union
    {
        const char* pid;
        ull number;
    } input_value;

    Value(ull number_);
    Value(const char* pid_);
    
    virtual ~Value();

    Register* return_register;
};

/* ============================================ */
/*                  VALUES                      */
/* ============================================ */

struct Constant : public Value
{
    Constant(ull number);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

/* ============================================ */


struct Identifier : public Value
{
    enum VAR_TYPE
    {
        VT_VARIABLE,
        VT_ARRAY_NUM_INDEXED,
        VT_ARRAY_VAR_INDEXED
    } var_type;

    union
    {
        const char* pid;
        ull number;
    } index_value;

    /** Set it to true if compiler should check if this identifier
     *  was initialized before you can refer to it.
     */
    bool init_check;

    Identifier(const char* pid, VAR_TYPE vt, bool init_check_);
    virtual ~Identifier();
};

/* ============================================ */
/*                 IDENTIFIERS
/* ============================================ */

struct Variable : public Identifier
{
    Variable(const char* pid);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct ArrayFieldNumIndexed : public Identifier
{
    ArrayFieldNumIndexed(const char* pid, ull index_number);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct ArrayFieldVarIndexed : public Identifier
{
    ArrayFieldVarIndexed(const char* pid, const char* index_pid);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

/* ============================================ */


struct Address : public Block
{
    Identifier* identifier;

    Address(Identifier* ident);
    virtual ~Address();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    Register* return_register;
};

/* ============================================ */


/** Represents a Conditional Expression */
struct Condition : public Block
{
    Value* value1;
    Value* value2;

    Condition(Value* val1, Value* val2);
    virtual ~Condition();

    /** Interpretation of the return value:
    *  return value is equal to 0 if
    *  and only if the condition evaluates
    *  to true; the return value is non-zero
    *  otherwise.
    */
    Register* return_register;
};

/* ============================================ */
/*                 CONDITIONS
/* ============================================ */

struct LessThan : public Condition
{
    LessThan(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct GreaterThan : public Condition
{
    GreaterThan(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct LessThanOrEqualTo : public Condition
{
    LessThanOrEqualTo(Value* val1, Value* val2);
    
    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct GreaterThanOrEqualTo : public Condition
{
    GreaterThanOrEqualTo(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct EqualTo : public Condition
{
    EqualTo(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct NotEqualTo : public Condition
{
    NotEqualTo(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

/* ============================================ */


struct Expression : public Block
{
    Value* value1;
    Value* value2;

    Expression(Value* val);
    Expression(Value* val1, Value* val2);
    virtual ~Expression();

    Register* return_register;
};

/* ============================================ */
/*                EXPRESSIONS
/* ============================================ */

struct ValueReference : public Expression
{
    ValueReference(Value* val);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct Addition : public Expression
{
    Addition(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct Subtraction : public Expression
{
    Subtraction(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct Multiplication : public Expression
{
    Multiplication(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct Division : public Expression
{
    Division(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

struct Modulo : public Expression
{
    Modulo(Value* val1, Value* val2);

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;
};

/* ============================================ */


struct Command : public Block
{
    Command(StatisticsContext* stat_context = nullptr);
    virtual ~Command() = 0;
    virtual void debugPrint(uint8_t level = 0) const = 0;

    protected:
    void _freeCommandBlock(std::vector<Command*>*& cmd_block);
    StatisticsContext* stats;
};

/* ============================================ */
/*                  COMMANDS
/* ============================================ */

struct Assignment : public Command
{
    Identifier* identifier;
    Expression* expression;

    Assignment(Identifier* ident, Expression* expr, StatisticsContext* stat_context = nullptr);
    virtual ~Assignment();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    virtual void debugPrint(uint8_t level = 0) const;
    virtual ull getSize() const;
};

struct IfElse : public Command
{
    Condition* condition;
    std::vector<Command*>* commands_if;
    std::vector<Command*>* commands_else;

    IfElse(Condition* cond, std::vector<Command*>* cmd_if, std::vector<Command*>* cmd_else,
           StatisticsContext* stat_context = nullptr);
    virtual ~IfElse();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    virtual void debugPrint(uint8_t level = 0) const;
    virtual ull getSize() const;
};

struct If : public Command
{
    Condition* condition;
    std::vector<Command*>* commands_if;

    If(Condition* cond, std::vector<Command*>* cmd_if,
       StatisticsContext* stat_context = nullptr);
    virtual ~If();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    virtual void debugPrint(uint8_t level = 0) const;
    virtual ull getSize() const;
};

struct While : public Command
{
    Condition* condition;
    std::vector<Command*>* commands_if;

    While(Condition* cond, std::vector<Command*>* cmd_if,
          StatisticsContext* stat_context = nullptr);
    virtual ~While();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    virtual void debugPrint(uint8_t level = 0) const;
    virtual ull getSize() const;
};

struct Repeat : public Command
{
    Condition* condition;
    std::vector<Command*>* commands_if;

    Repeat(Condition* cond, std::vector<Command*>* cmd_if,
           StatisticsContext* stat_context = nullptr);
    virtual ~Repeat();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    virtual void debugPrint(uint8_t level = 0) const;
    virtual ull getSize() const;
};

struct ForTo : public Command
{
    ValueReference* from_ref;
    ValueReference* to_ref;
    LessThanOrEqualTo* to_cond;
    Variable* iter;
    Variable* to_var;
    std::vector<Command*>* commands;

    ForTo(const char* pid, Value* from_val, Value* to_val, std::vector<Command*>* cmds,
          StatisticsContext* stat_context = nullptr);
    virtual ~ForTo();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    virtual void debugPrint(uint8_t level = 0) const;
    virtual ull getSize() const;
};

struct ForDownTo : public Command
{
    ValueReference* from_ref;
    ValueReference* to_ref;
    GreaterThanOrEqualTo* to_cond;
    Variable* iter;
    Variable* to_var;
    std::vector<Command*>* commands;

    ForDownTo(const char* pid, Value* from_val, Value* to_val, std::vector<Command*>* cmds,
              StatisticsContext* stat_context = nullptr);
    virtual ~ForDownTo();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    virtual void debugPrint(uint8_t level = 0) const;
    virtual ull getSize() const;
};

struct Read : public Command
{
    Identifier* identifier;

    Read(Identifier* ident, StatisticsContext* stat_context = nullptr);
    virtual ~Read();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    virtual void debugPrint(uint8_t level = 0) const;
    virtual ull getSize() const;
};

struct Write : public Command
{
    Value* value;

    Write(Value* val, StatisticsContext* stat_context = nullptr);
    virtual ~Write();

    virtual std::string getCode(MemoryManager& mgr, ull& length);
    virtual std::string getName() const;
    virtual uint8_t getClobberCount() const;

    virtual void debugPrint(uint8_t level = 0) const;
    virtual ull getSize() const;
};

/* ============================================ */


#endif