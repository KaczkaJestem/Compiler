#include "MemoryManager.hpp"

MemoryManager::MemoryManager(bool debug_log)
{
    _debug_log = debug_log;

    std::string names[] = {"a", "b", "c", "d", "e", "f"};

    for (int i = 0; i < reg_count; i++)
    {
        registers[i].tag = Register::CONTENT_TAG::CT_CONST;
        registers[i].content.constant = 0;
        registers[i].name = names[i];
        registers[i].locked = false;
    }
}

MemoryManager::~MemoryManager()
{
    cleanup();
}

void MemoryManager::declareInt(const char* identifier, ull line_no)
{
    _log("Declaring variable " + string(identifier));
    bool declared = true;

    try{ _findSymbol(identifier, line_no); }
    catch (MemoryException& exc)
    { declared = false; }

    if (declared)
        throw MemoryException("Redeclaration of identifier: " + std::string(identifier), line_no);

    Symbol* symbol = new Symbol;
    symbol->pidentifier = identifier;
    symbol->length = 0;
    symbol->begin_index = 0;
    symbol->initialized = false;
    symbol->iterator = false;

    try
    {
        _allocate(symbol);
    }
    catch(MemoryException& exc)
    {
        throw exc;
    }

    _log("Registering " + string(identifier));

    symbol_table.push_back(symbol);
    
    _log(string(identifier) + " declared.");
}

void MemoryManager::declareArray(const char* identifier, ull lower_bound, 
                                 ull upper_bound, ull line_no)
{
    _log("Declaring array " + string(identifier));
    bool declared = true;

    try{ _findSymbol(identifier, line_no); }
    catch (MemoryException& exc)
    { declared = false; }

    if (declared)
        throw MemoryException("Redeclaration of " + std::string(identifier) 
        + " identifier!", line_no);

    if (lower_bound > upper_bound)
    {
         _log("Array bounds mismatch.");

        throw MemoryException("Lower bound of array " + std::string(identifier)
        + " is greater than the upper bound!", line_no);
    }

    Symbol* symbol = new Symbol;
    symbol->pidentifier = identifier;
    symbol->length = upper_bound - lower_bound + 1;
    symbol->begin_index = lower_bound;
    symbol->initialized = false;
    symbol->iterator = false;
    
    try
    {
        _allocate(symbol);
    }
    catch(MemoryException& exc)
    {
        throw exc;
    }

    _log("Registering " + string(identifier));

    symbol_table.push_back(symbol);

    _log(string(identifier) + " declared.");
}

void MemoryManager::_allocate(Symbol* symbol)
{
    if (symbol->length == 0)
        _log("Allocating space for variable " + string(symbol->pidentifier));
    else
        _log("Allocating space for array " + string(symbol->pidentifier));

    ull offset;

    try
    {
        offset = _findMemorySpace();
    }
    catch(MemoryException& exc)
    {
        throw exc;
    }

    symbol->location.tag = Location::MEMORY;
    symbol->location.ptr.mem_offset = offset;

    if (symbol->location.tag == Location::MEMORY)
        _log("Allocated in MEMORY at: " + to_string(offset)
        + ", len: " + to_string(symbol->length));
    else
        _log("Allocated in REGISTER " + symbol->location.ptr.reg->name);
}

void MemoryManager::free(const char* identifier)
{
    _log("Freeing " + string(identifier));

    Symbol* symbol = _popSymbol(identifier);
    delete symbol;

    _log("Freed.");
}

void MemoryManager::cleanup()
{
    _log("Clearing Symbol Table");
    for(std::vector<Symbol*>::iterator it = symbol_table.begin();
        it != symbol_table.end(); ++it)
        delete *it;
    
    symbol_table.clear();

    _log("Done.");
}

Location MemoryManager::getVariableLocation(const char* pid, ull line_no) const
{
    Symbol* sym = _findSymbol(pid, line_no);

    if(sym->length != 0)
        throw MemoryException("Array " + std::string(pid) 
        + " cannot be used as a single variable!", line_no);

    return sym->location;
}

Location MemoryManager::getArrayFieldLocation(const char* pid, uint64_t index, 
                                              ull line_no) const
{
    Symbol* sym = _findSymbol(pid, line_no);

    if(sym->length == 0)
        throw MemoryException("Variable " + std::string(pid) 
        + " cannot be used as an array!", line_no);

    Location loc = sym->location;
    
    if (loc.tag == Location::MEMORY)
    {
        loc.ptr.mem_offset += index - sym->begin_index;
    }

    return loc;
}

Location MemoryManager::getArrayLocation(const char* pid, ull line_no) const
{
    Symbol* sym = _findSymbol(pid, line_no);
    
    if(sym->length == 0)
        throw MemoryException("Variable " + std::string(pid) 
        + " cannot be used as an array!", line_no);

    return sym->location;
}

uint64_t MemoryManager::getBeginIndex(const char* pid, ull line_no) const
{
    Symbol* sym = _findSymbol(pid, line_no);

    if(sym->length == 0)
        throw MemoryException("Asked for begin index of " + std::string(pid) 
        + ", which is not an array!", line_no);

    return sym->begin_index;
}

void MemoryManager::markInitialized(const char* pid, ull line_no)
{
    Symbol* sym = _findSymbol(pid, line_no);
    sym->initialized = true;
}

bool MemoryManager::isInitialized(const char* pid, ull line_no) const
{
    Symbol* sym = _findSymbol(pid, line_no);
    return sym->initialized;
}

void MemoryManager::markIterator(const char* pid, ull line_no)
{
    Symbol* sym = _findSymbol(pid, line_no);
    sym->iterator = true;
}

bool MemoryManager::isIterator(const char* pid, ull line_no) const
{
    Symbol* sym = _findSymbol(pid, line_no);
    return sym->iterator;
}

Register* MemoryManager::assignRegister()
{
     for(uint8_t it = 0; it < reg_count; it++)
        if (!registers[it].locked)
        {
            registers[it].locked = true;
            return &registers[it];
        }

    throw MemoryException("Cannot assign register - all registers are locked!", 0);
}

void MemoryManager::freeRegister(std::string name)
{
    for(uint8_t it = 0; it < reg_count; it++)
        if (registers[it].name == name)
            registers[it].locked = false;
}

void MemoryManager::freeAllRegisters()
{
    for(uint8_t it = 0; it < reg_count; it++)
        registers[it].locked = false;
}

Symbol* MemoryManager::_findSymbol(const char* identifier, ull line_no) const
{
    _log("Searching for symbol " + string(identifier));

    std::vector<Symbol*>::const_iterator it;
    for(it = symbol_table.begin(); it != symbol_table.end(); ++it)
    {
        if(std::strcmp((*it)->pidentifier, identifier) == 0)
        {
            _log("Found.");
            return *it;
        }
    }

    _log("Not found.");
    throw MemoryException("Declaration of symbol " + string(identifier)
        + " not found!", line_no);
}

Symbol* MemoryManager::_popSymbol(const char* identifier)
{
    std::vector<Symbol*>::iterator it;
    for(it = symbol_table.begin(); it != symbol_table.end(); ++it)
    {
        if(std::strcmp((*it)->pidentifier, identifier) == 0)
        {
            Symbol* popped = *it;
            symbol_table.erase(it);

            return popped;
        }
    }

    return nullptr;
}

ull MemoryManager::_findMemorySpace() const
{
    _log("Searching for space");
    ull space_index = 0;

    // Need to think about it:
    if (symbol_table.size() != 0)
    {
        Symbol* last = symbol_table.back();
        ull length = max(last->length, (uint64_t) 1);
        space_index = last->location.ptr.mem_offset + length;
    }

    _log("Found free space at: " + to_string(space_index));
    return space_index;

    // throw MemoryException("Cannot find enough memory space!");
}

void MemoryManager::_printSymbolTable() const
{
    cout << "(*) Symbol Table content:" << endl;
    cout << "(*) ---------------------" << endl;
    
    for(std::vector<Symbol*>::const_iterator it = symbol_table.begin();
        it != symbol_table.end(); ++it)
    {
        cout << "(*) " << (*it)->pidentifier << ", len: "
        << (*it)->length << ", begin_index: " << (*it)->begin_index
        << ", location: ";

        if ((*it)->location.tag == Location::MEMORY)
            cout << "MEM, offset: " << (*it)->location.ptr.mem_offset;
        else
            cout << "REG " << (*it)->location.ptr.reg->name;

        cout << endl;
    }

    cout << "(*) ---------------------" << endl;
}

void MemoryManager::_log(std::string msg) const
{
    if(_debug_log)
        cout << "(+) " << msg << endl;
}