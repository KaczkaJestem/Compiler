#ifndef __TRANSLATOR_HPP__
#define __TRANSLATOR_HPP__

#include <iostream>
#include <fstream>
#include <vector>
#include <cstring>

#include "Symbol.hpp"
#include "StatisticsContext.hpp"
#include "Blocks.hpp"
#include "MemoryManager.hpp"

using namespace std;

class Translator
{
    public:
        Translator(StatisticsContext& stat_context, bool debug_log = false);
        ~Translator();

        void declareInt(const char* identifier, ull line_no);
        void declareArray(const char* identifier, ull lower_bound, ull upper_bound, ull line_no);

        void translate(vector<Command*>* program);
        bool saveToFile(const char* filename) const;
        void cleanup();
        void clear_all();
        
    private:
        MemoryManager memory_mgr;
        StatisticsContext& statistics;
        vector<Command*>* _program_tree;
        std::string _assembly_code;
        bool _debug_log;

        void _codePercentageWatch();
        void _printSyntaxTree() const;
        void _log(std::string msg) const;
};

#endif