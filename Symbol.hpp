#ifndef __SYMBOL_HPP__
#define __SYMBOL_HPP__

#include <cstdint>
#include <string>

typedef unsigned long long ull;

struct Symbol;

struct Register
{
    std::string name;
    bool locked;

    enum CONTENT_TAG
    {
        CT_SYMBOL,
        CT_CONST
    } tag;

    union Content
    {
        Symbol* symbol;
        unsigned long long constant;   
    } content;

};

struct Location
{
    enum
    {
        REGISTER,
        MEMORY
    } tag;

    union
    {
        Register* reg;
        uint64_t mem_offset;
    } ptr;
};

struct Symbol
{
    const char* pidentifier;
    Location location;
    uint64_t length;
    uint64_t begin_index;
    bool initialized;
    bool iterator;
};

#endif