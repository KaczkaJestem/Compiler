#ifndef __EXCEPTIONS_HPP__
#define __EXCEPTIONS_HPP__

#include <string>
typedef unsigned long long ull;

struct Exception : public std::exception
{
    Exception(std::string msg, ull line_no = 0);
    virtual const char* what() const noexcept = 0;

    protected:
        std::string _msg;
        ull _line_no;
};

struct TranslatorException : public Exception
{
    TranslatorException(std::string msg, ull line_no);
    virtual const char* what() const noexcept;
};

struct MemoryException : public Exception
{
    MemoryException(std::string msg, ull line_no);
    virtual const char* what() const noexcept;
};

#endif