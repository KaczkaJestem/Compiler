#include "Blocks.hpp"

/* ========================================= */
/*             CLOBBER UTILITIES             */
/* ========================================= */

void Block::assignClobbers(MemoryManager& mgr)
{
    for (uint8_t i = 0; i < this->getClobberCount(); i++)
        clobber_reg.push_back(mgr.assignRegister());
}

void Block::freeClobbers(MemoryManager& mgr)
{
    for (uint8_t i = 0; i < this->getClobberCount(); i++)
        mgr.freeRegister(clobber_reg[i]->name);
}


/* ========================================= */
/*          C-TORS / D-TORS SECTION          */
/* ========================================= */

Value::Value(ull number_)
{
    type = T_CONSTANT;
    input_value.number = number_;
}

Value::Value(const char* pid_)
{
    type = T_VARIABLE;
    input_value.pid = pid_;
}

Value::~Value()
{
    if (type == T_VARIABLE)
        delete input_value.pid;
}

Constant::Constant(ull number)
    : Value(number)
{
}

Identifier::Identifier(const char* pid, VAR_TYPE vt, bool init_check_)
    : Value(pid)
{
    init_check = init_check_;
    var_type = vt;
}

Identifier::~Identifier()
{
    if (var_type == VT_ARRAY_VAR_INDEXED)
        delete index_value.pid;
}

Variable::Variable(const char* pid)
    : Identifier(pid, VT_VARIABLE, true)
{
}

ArrayFieldVarIndexed::ArrayFieldVarIndexed(const char* pid, const char* index_pid)
    : Identifier(pid, VT_ARRAY_VAR_INDEXED, false)
{
    index_value.pid = index_pid;
}

ArrayFieldNumIndexed::ArrayFieldNumIndexed(const char* pid, ull index_number)
    : Identifier(pid, VT_ARRAY_NUM_INDEXED, false)
{
    index_value.number = index_number;
}

Address::Address(Identifier* ident)
{
    identifier = ident;
}

Address::~Address()
{
    // Address is not a built-in language command,
    // so we don't delete identifier
}

Condition::Condition(Value* val1, Value* val2)
{
    value1 = val1;
    value2 = val2;
}

Condition::~Condition()
{
    delete value1, value2;
}

LessThan::LessThan(Value* val1, Value* val2)
    : Condition(val1, val2)
{

}

GreaterThan::GreaterThan(Value* val1, Value* val2)
    : Condition(val1, val2)
{

}

LessThanOrEqualTo::LessThanOrEqualTo(Value* val1, Value* val2)
    : Condition(val1, val2)
{

}

GreaterThanOrEqualTo::GreaterThanOrEqualTo(Value* val1, Value* val2)
    : Condition(val1, val2)
{

}

EqualTo::EqualTo(Value* val1, Value* val2)
    : Condition(val1, val2)
{

}

NotEqualTo::NotEqualTo(Value* val1, Value* val2)
    : Condition(val1, val2)
{

}

Expression::Expression(Value* val1, Value* val2)
{
    value1 = val1;
    value2 = val2;
}

Expression::Expression(Value* val)
{
    value1 = val;
    value2 = nullptr;
}

Expression::~Expression()
{
    if(value2)
        delete value1, value2;
    else 
        delete value1;
}

ValueReference::ValueReference(Value* val)
    : Expression(val)
{
    
}

Addition::Addition(Value* val1, Value* val2)
    : Expression(val1, val2)
{

}

Subtraction::Subtraction(Value* val1, Value* val2)
    : Expression(val1, val2)
{

}

Multiplication::Multiplication(Value* val1, Value* val2)
    : Expression(val1, val2)
{

}

Division::Division(Value* val1, Value* val2)
    : Expression(val1, val2)
{

}

Modulo::Modulo(Value* val1, Value* val2)
    : Expression(val1, val2)
{

}

Command::Command(StatisticsContext* stat_context)
{
    stats = stat_context;
    if (stats)
        stats->commands_count++;
}

Command::~Command()
{

}

void Command::_freeCommandBlock(std::vector<Command*>*& cmd_block)
{
    for(std::vector<Command*>::iterator it = cmd_block->begin(); it != cmd_block->end(); ++it)
        delete *it;

    delete cmd_block;
}

Assignment::Assignment(Identifier* ident, Expression* expr, StatisticsContext* stat_context)
    : Command(stat_context)
{
    identifier = ident;
    expression = expr;
}

Assignment::~Assignment()
{
    delete identifier, expression;
}

IfElse::IfElse(Condition* cond, std::vector<Command*>* cmd_if, std::vector<Command*>* cmd_else,
               StatisticsContext* stat_context)
    : Command(stat_context)
{
    condition = cond;
    commands_if = cmd_if;
    commands_else = cmd_else;
}

IfElse::~IfElse()
{
    delete condition;
    _freeCommandBlock(commands_if);
    _freeCommandBlock(commands_else);
}

If::If(Condition* cond, std::vector<Command*>* cmd_if, StatisticsContext* stat_context)
    : Command(stat_context)
{
    condition = cond;
    commands_if = cmd_if;
}

If::~If()
{
    delete condition;
    _freeCommandBlock(commands_if);
}

While::While(Condition* cond, std::vector<Command*>* cmd_if, StatisticsContext* stat_context)
    : Command(stat_context)
{
    condition = cond;
    commands_if = cmd_if;
}

While::~While()
{
    delete condition;
    _freeCommandBlock(commands_if);
}

Repeat::Repeat(Condition* cond, std::vector<Command*>* cmd_if, StatisticsContext* stat_context)
    : Command(stat_context)
{
    condition = cond;
    commands_if = cmd_if;
}

Repeat::~Repeat()
{
    delete condition;
    _freeCommandBlock(commands_if);
}

ForTo::ForTo(const char* pid, Value* from_val, Value* to_val, std::vector<Command*>* cmds,
             StatisticsContext* stat_context)
    : Command(stat_context)
{
    iter = new Variable(pid);
    iter->line_no = line_no;
    from_ref = new ValueReference(from_val);
    from_ref->line_no = line_no;

    std::string to_pid_str = "#to";
    to_pid_str += iter->input_value.pid;
    const char* to_pid = new char[to_pid_str.length()];
    strcpy((char*) to_pid, to_pid_str.c_str());
    
    to_var = new Variable(to_pid);
    to_var->line_no = line_no;
    to_ref = new ValueReference(to_val);
    to_ref->line_no = line_no;

    to_cond = new LessThanOrEqualTo(iter, to_var);
    to_cond->line_no = line_no;
    commands = cmds;
}

ForTo::~ForTo()
{
    delete from_ref, to_ref, to_var, to_cond, iter;
    _freeCommandBlock(commands);
}

ForDownTo::ForDownTo(const char* pid, Value* from_val, Value* to_val, std::vector<Command*>* cmds,
                     StatisticsContext* stat_context)
    : Command(stat_context)
{
    iter = new Variable(pid);
    iter->line_no = line_no;
    from_ref = new ValueReference(from_val);
    from_ref->line_no = line_no;

    std::string to_pid_str = "#to";
    to_pid_str += iter->input_value.pid;
    const char* to_pid = new char[to_pid_str.length()];
    strcpy((char*) to_pid, to_pid_str.c_str());

    to_var = new Variable(to_pid);
    to_var->line_no = line_no;
    to_ref = new ValueReference(to_val);
    to_var->line_no = line_no;

    to_cond = new GreaterThanOrEqualTo(iter, to_val);
    to_cond->line_no = line_no;
    commands = cmds;
}

ForDownTo::~ForDownTo()
{
    delete from_ref, to_ref, to_var, to_cond, iter;
    _freeCommandBlock(commands);
}

Read::Read(Identifier* ident, StatisticsContext* stat_context)
    : Command(stat_context)
{
    identifier = ident;
}

Read::~Read()
{
    delete identifier;
}

Write::Write(Value* val, StatisticsContext* stat_context)
    : Command(stat_context)
{
    value = val;
}

Write::~Write()
{
    delete value;
}


/* ========================================= */
/*               DEBUG SECTION               */
/* ========================================= */

void Assignment::debugPrint(uint8_t level) const
{
    std::cout << "(&) " << std::string((int) level, ' ') << getName() << std::endl;
}

void If::debugPrint(uint8_t level) const
{ 
    std::cout << "(&) " << std::string((int) level, ' ') << getName() << std::endl;
    std::cout << "(&) "  << std::string((int) level, ' ') << "-> if_block: " << std::endl;
    
    for(std::vector<Command*>::const_iterator it = commands_if->begin();
        it != commands_if->end(); ++it)
    {
        (*it)->debugPrint(level + 1);
    }
}

void IfElse::debugPrint(uint8_t level) const
{
    std::cout << "(&) " << std::string((int) level, ' ') << getName() << std::endl;
    std::cout << "(&) "  << std::string((int) level, ' ') << "-> if_block: " << std::endl;
    
    for(std::vector<Command*>::const_iterator it = commands_if->begin();
        it != commands_if->end(); ++it)
    {
        (*it)->debugPrint(level + 1);
    }

    std::cout << "(&) "  << std::string((int) level, ' ') << "-> else_block: " << std::endl;

    for(std::vector<Command*>::const_iterator it = commands_else->begin();
        it != commands_else->end(); ++it)
    {
        (*it)->debugPrint(level + 1);
    }
}

void ForTo::debugPrint(uint8_t level) const
{
    std::cout << "(&) " << std::string((int) level, ' ') << getName() << std::endl;
    std::cout << "(&) "  << std::string((int) level, ' ') << "-> for_block: " << std::endl;
    
    for(std::vector<Command*>::const_iterator it = commands->begin();
        it != commands->end(); ++it)
    {
        (*it)->debugPrint(level + 1);
    }
}

void ForDownTo::debugPrint(uint8_t level) const
{
    std::cout << "(&) " << std::string((int) level, ' ') << getName() << std::endl;
    std::cout << "(&) "  << std::string((int) level, ' ') << "-> for_block: " << std::endl;
    
    for(std::vector<Command*>::const_iterator it = commands->begin();
        it != commands->end(); ++it)
    {
        (*it)->debugPrint(level + 1);
    }
}

void While::debugPrint(uint8_t level) const
{
    std::cout << "(&) " << std::string((int) level, ' ') << getName() << std::endl;
    std::cout << "(&) "  << std::string((int) level, ' ') << "-> while_block: " << std::endl;
    
    for(std::vector<Command*>::const_iterator it = commands_if->begin();
        it != commands_if->end(); ++it)
    {
        (*it)->debugPrint(level + 1);
    }
}

void Repeat::debugPrint(uint8_t level) const
{
    std::cout << "(&) " << std::string((int) level, ' ') << getName() << std::endl;
    std::cout << "(&) "  << std::string((int) level, ' ') << "-> repeat_block: " << std::endl;
    
    for(std::vector<Command*>::const_iterator it = commands_if->begin();
        it != commands_if->end(); ++it)
    {
        (*it)->debugPrint(level + 1);
    }
}

void Read::debugPrint(uint8_t level) const
{
    std::cout << "(&) " << std::string((int) level, ' ') << getName() << std::endl;
}

void Write::debugPrint(uint8_t level) const
{
    std::cout << "(&) " << std::string((int) level, ' ') << getName() << std::endl;
}


/* ========================================= */
/*               NAMES SECTION               */
/* ========================================= */

std::string Constant::getName() const               { return "Constant"; }
std::string Variable::getName() const               { return "Variable"; }
std::string ArrayFieldNumIndexed::getName() const   { return "ArrayFieldNumIndexed"; }
std::string ArrayFieldVarIndexed::getName() const   { return "ArrayFieldVarIndexed"; }
std::string Address::getName() const                { return "Address"; }
std::string LessThan::getName() const               { return "LessThan"; }
std::string GreaterThan::getName() const            { return "GreaterThan"; }
std::string LessThanOrEqualTo::getName() const      { return "LessThanOrEqualTo"; }
std::string GreaterThanOrEqualTo::getName() const   { return "GreaterThanOrEqualTo"; }
std::string EqualTo::getName() const                { return "EqualTo"; }
std::string NotEqualTo::getName() const             { return "NotEqualTo"; }
std::string ValueReference::getName() const         { return "ValueReference"; }
std::string Addition::getName() const               { return "Addition"; }
std::string Subtraction::getName() const            { return "Subtraction"; }
std::string Multiplication::getName() const         { return "Multiplication"; }
std::string Division::getName() const               { return "Division"; }
std::string Modulo::getName() const                 { return "Modulo"; }
std::string Assignment::getName() const             { return "Assignment"; }
std::string If::getName() const                     { return "If"; }
std::string IfElse::getName() const                 { return "IfElse"; }
std::string ForTo::getName() const                  { return "ForTo"; }
std::string ForDownTo::getName() const              { return "ForDownTo"; }
std::string While::getName() const                  { return "While"; }
std::string Repeat::getName() const                 { return "Repeat"; }
std::string Read::getName() const                   { return "Read"; }
std::string Write::getName() const                  { return "Write"; }


/* ========================================= */
/*              CLOBBERS SECTION             */
/* ========================================= */

uint8_t Constant::getClobberCount() const               { return 0; }
uint8_t Variable::getClobberCount() const               { return 0; }
uint8_t ArrayFieldNumIndexed::getClobberCount() const   { return 0; }
uint8_t ArrayFieldVarIndexed::getClobberCount() const   { return 0; }
uint8_t Address::getClobberCount() const                { return 0; }
uint8_t LessThan::getClobberCount() const               { return 1; }
uint8_t GreaterThan::getClobberCount() const            { return 1; }
uint8_t LessThanOrEqualTo::getClobberCount() const      { return 1; }
uint8_t GreaterThanOrEqualTo::getClobberCount() const   { return 1; }
uint8_t EqualTo::getClobberCount() const                { return 2; }
uint8_t NotEqualTo::getClobberCount() const             { return 2; }
uint8_t ValueReference::getClobberCount() const         { return 0; }
uint8_t Addition::getClobberCount() const               { return 1; }
uint8_t Subtraction::getClobberCount() const            { return 1; }
uint8_t Multiplication::getClobberCount() const         { return 2; }
uint8_t Division::getClobberCount() const               { return 4; }
uint8_t Modulo::getClobberCount() const                 { return 4; }
uint8_t Assignment::getClobberCount() const             { return 0; }
uint8_t If::getClobberCount() const                     { return 0; }
uint8_t IfElse::getClobberCount() const                 { return 0; }
uint8_t ForTo::getClobberCount() const                  { return 0; }
uint8_t ForDownTo::getClobberCount() const              { return 0; }
uint8_t While::getClobberCount() const                  { return 0; }
uint8_t Repeat::getClobberCount() const                 { return 0; }
uint8_t Read::getClobberCount() const                   { return 0; }
uint8_t Write::getClobberCount() const                  { return 0; }


/* ========================================= */
/*               SIZE SECTION                */
/* ========================================= */

ull Block::getSize() const                  { return 0; }
ull Assignment::getSize() const             { return 1; }

ull If::getSize() const
{
    ull nested = 0;

    for(vector<Command*>::const_iterator it = commands_if->begin();
        it != commands_if->end(); it++)
        nested += (*it)->getSize();

    return 1 + nested; 
}

ull IfElse::getSize() const
{ 
    ull nested = 0;

    for(vector<Command*>::const_iterator it = commands_if->begin();
        it != commands_if->end(); it++)
        nested += (*it)->getSize();
    
    for(vector<Command*>::const_iterator it = commands_else->begin();
        it != commands_else->end(); it++)
        nested += (*it)->getSize();

    return 1 + nested; 
}

ull ForTo::getSize() const
{ 
    ull nested = 0;

    for(vector<Command*>::const_iterator it = commands->begin();
        it != commands->end(); it++)
        nested += (*it)->getSize();

    return 1 + nested; 
}

ull ForDownTo::getSize() const
{ 
    ull nested = 0;

    for(vector<Command*>::const_iterator it = commands->begin();
        it != commands->end(); it++)
        nested += (*it)->getSize();

    return 1 + nested; 
}

ull While::getSize() const
{ 
    ull nested = 0;

    for(vector<Command*>::const_iterator it = commands_if->begin();
        it != commands_if->end(); it++)
        nested += (*it)->getSize();

    return 1 + nested; 
}

ull Repeat::getSize() const
{ 
    ull nested = 0;

    for(vector<Command*>::const_iterator it = commands_if->begin();
        it != commands_if->end(); it++)
        nested += (*it)->getSize();

    return 1 + nested; 
}

ull Read::getSize() const                   { return 1; }
ull Write::getSize() const                  { return 1; }