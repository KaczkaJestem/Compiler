#ifndef __TOKEN_H__
#define __TOKEN_H__

typedef unsigned long long ull;

// Struct to contain token data receieved from lexer
struct Token
{
    union
    {
        ull ival;
        const char* sval;
    } data;

    ull line_no;
};
#endif