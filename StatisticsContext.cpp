#include "StatisticsContext.hpp"

StatisticsContext::StatisticsContext()
{
    commands_count = 0;
    commands_parsed = 0;
    _monitor = false;
}

void StatisticsContext::_displayPercentage(ull current, ull total) const
{
    short percent = 100 * double(current) / double(total);
    cout << "\r" << "[" << std::string(percent / 5, '=')
         << std::string(100 / 5 - percent / 5, ' ') << "]";
    cout << percent << "%" << " [Processed " << current << " out of " << total << "]";
    std::cout.flush();
}

void StatisticsContext::displayParsedCmdsPercentage() const
{
    _displayPercentage(commands_parsed, commands_count);
}

void StatisticsContext::monitorParsedCmdsPrecentage()
{
    _monitor = true;
    _monitor_thread = std::thread(&StatisticsContext::_monitorParsedCmds, this);
}

void StatisticsContext::stopMonitoring()
{
    if (_monitor)
    {
        _monitor = false;
        _monitor_thread.join();
        displayParsedCmdsPercentage();
        cout << endl;
    }
}

void StatisticsContext::_monitorParsedCmds()
{
    while (_monitor)
    {
        displayParsedCmdsPercentage();
    }
}