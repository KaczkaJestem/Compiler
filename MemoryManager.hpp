#ifndef __MEMORY_MANAGER_HPP__
#define __MEMORY_MANAGER_HPP__

#include <vector>
#include <string>
#include <cstring>
#include <iostream>
#include "Exceptions.hpp"
#include "Symbol.hpp"

using namespace std;

class MemoryManager
{
    public:
        MemoryManager(bool debug_log = false);
        ~MemoryManager();

        void declareInt(const char* identifier, ull line_no);
        void declareArray(const char* identifier, ull lower_bound, ull upper_bound, ull line_no);
    
        void free(const char* identifier);
        void cleanup();

        Location getVariableLocation(const char* pid, ull line_no) const;
        Location getArrayFieldLocation(const char* pid, uint64_t index, ull line_no) const;
        Location getArrayLocation(const char* pid, ull line_no) const;
        uint64_t getBeginIndex(const char* pid, ull line_no) const;

        void markInitialized(const char* pid, ull line_no);
        bool isInitialized(const char* pid, ull line_no) const;

        void markIterator(const char* pid, ull line_no);
        bool isIterator(const char* pid, ull line_no) const;

        Register* assignRegister();
        void freeRegister(std::string);
        void freeAllRegisters();

    private:
        vector<Symbol*> symbol_table;
        static const uint8_t reg_count = 6;
        Register registers[reg_count];

        bool _debug_log;

        Symbol* _findSymbol(const char* identifier, ull line_no) const;
        Symbol* _popSymbol(const char* identifier);
         
        void _allocate(Symbol* symbol);
        ull _findMemorySpace() const;

        void _printSymbolTable() const;
        void _log(std::string msg) const;

        friend class Translator;
};

#endif