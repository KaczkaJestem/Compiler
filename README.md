### Autor: Adrian Kiełbowicz
### Nr indeksu: 250081


#### Opis
Jest to projekt kompilatora wykonywany na zaliczenie
kursu JFiTT (2020/21). Jest on zgodny ze specyfikacją
zawartą na liście laboratoryjnej nr 4.

Kod programu napisany jest w języku C/C++ i składa się z plików
nagłówkowych o rozszerzeniu .hpp oraz źródłowych o rozszerzeniu .cpp.
Stanowią one implementację działania kompilatora i zawierają kod
używanych przez niego funkcji, obiektów i struktur.
Plik lexer.l jest plikiem rozpoznawanym przez lexer
biblioteki FLEX i służy zdefiniowaniu reguł analizy leksykalnej kompilowanych
kodów.
Plik gram.y jest plikiem charakterystycznym dla parsera Bison
i zawiera on przede wszystkim reguły gramatyki języka, który
znalazł się w specyfikacji zadania. Poza tym znajduje się w nim
definicja funkcji main, od której program zaczyna pracę.
Dodany został również plik Makefile, który umożliwia budowanie projektu oraz
ewentualne jego oczyszczanie.

#### Wymagania:

* g++ (wersja: 5.4.0 20160609)
* flex (wersja: 2.6.0)
* GNU Bison (wersja: 3.0.4)


#### Budowanie:

Wykonujemy w katalogu głównym (w którym znajduje się plik Makefile) polecenie:

**$ make all**

W celu wyczyszczenia projektu należy w tym samym katalogu wywołać polecenie:

**$ make clean**

**Uwaga:** program korzysta ze standardu c++11 i bibliotek 
POSIX Thread (pthread) oraz Flex (fl). Zostały one uwzględnione w pliku Makefile.

#### Uruchomienie:

Wywołujemy program poleceniem:

**$ kompilator <nazwa pliku wejściowego> <nazwa pliku wyjściowego>**

Program zaczyta plik wejściowy (preferowane rozszerzenie takiego pliku to .imp)
i spróbuje przetłumaczyć go na assembler maszyny wirtualnej. W razie błędów
wypisze je na ekran i skończy działanie. Jeśli kompilacja powiedzie się,
utworzony zostanie plik wyjściowy zawierający kod assemblera.
