#ifndef __STATISTICCONTEXT_HPP___
#define __STATISTICCONTEXT_HPP___

#include <iostream>
#include <thread>

typedef unsigned long long ull;
using namespace std;

struct StatisticsContext
{
    public:
        StatisticsContext();

        mutable ull commands_count;
        mutable ull commands_parsed;

        void displayParsedCmdsPercentage() const;

        void monitorParsedCmdsPrecentage();
        void stopMonitoring();

    private:
        mutable bool _monitor;
        std::thread _monitor_thread;

        void _displayPercentage(ull current, ull total) const;
        void _monitorParsedCmds();
};

#endif