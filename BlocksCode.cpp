#include "Blocks.hpp"

std::string Constant::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;

    code += "RESET " + return_register->name + "\n";
    length++;
    
    if (input_value.number == 0)
        return code;

    for(ull i = floor(log2(input_value.number)); i > 0; i--)
    {
        if(input_value.number & ((ull) 1 << i))
        {
            code += "INC " + return_register->name + "\n";
            length++;
        }

        code += "SHL " + return_register->name + "\n";
        length++;
    }

    if(input_value.number & 1)
    {
        code += "INC " + return_register->name + "\n";
        length++;
    }

    return code;
}

std::string Address::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    Location loc;
    uint64_t begin_index;

    switch(identifier->var_type)
    {
        case Identifier::VAR_TYPE::VT_VARIABLE:
            loc = mgr.getVariableLocation(identifier->input_value.pid, this->line_no);
            break;
        case Identifier::VAR_TYPE::VT_ARRAY_NUM_INDEXED:
            loc = mgr.getArrayFieldLocation(identifier->input_value.pid,
                                            identifier->index_value.number, this->line_no);
            break;
        case Identifier::VAR_TYPE::VT_ARRAY_VAR_INDEXED:
            loc = mgr.getArrayLocation(identifier->input_value.pid, this->line_no);
            break;
    }

    Constant tmp_addr(loc.ptr.mem_offset);
    tmp_addr.line_no = line_no;
    tmp_addr.return_register = return_register;

    code += tmp_addr.getCode(mgr, length);

    if (identifier->var_type == Identifier::VAR_TYPE::VT_ARRAY_VAR_INDEXED)
    {
        Register* A = mgr.assignRegister();

        const char* pid_cpy = new char[strlen(identifier->index_value.pid)];
        strcpy((char*) pid_cpy, identifier->index_value.pid);

        Variable tmp_index_var(pid_cpy);
        tmp_index_var.line_no = line_no;
        tmp_index_var.return_register = A;

        code += tmp_index_var.getCode(mgr, length);

        code += "ADD " + return_register->name + " " + A->name + "\n";
        length++;

        uint64_t begin_offset = mgr.getBeginIndex(identifier->input_value.pid, this->line_no);
        Constant tmp_begin_offset(begin_offset);
        tmp_begin_offset.line_no = line_no;
        tmp_begin_offset.return_register = A;

        code += tmp_begin_offset.getCode(mgr, length);

        code += "SUB " + return_register->name + " " + A->name + "\n";
        length++;

        mgr.freeRegister(A->name);
    }

    return code;
}

std::string Variable::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;

    if(init_check && !mgr.isInitialized(this->input_value.pid, this->line_no))
        throw TranslatorException("Variable " + std::string(this->input_value.pid)
        + " is referred to before being initialized!", this->line_no);

    Address tmp_addr(this);
    tmp_addr.line_no = line_no;
    tmp_addr.return_register = return_register;
    
    code += tmp_addr.getCode(mgr, length);

    code += "LOAD " + return_register->name + " " + return_register->name + "\n";
    length++;

    return code;
}

std::string ArrayFieldNumIndexed::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    
    Address tmp_addr(this);
    tmp_addr.line_no = line_no;
    tmp_addr.return_register = return_register;
    
    code += tmp_addr.getCode(mgr, length);

    code += "LOAD " + return_register->name + " " + return_register->name + "\n";
    length++;

    return code;
}

std::string ArrayFieldVarIndexed::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;

    Address tmp_addr(this);
    tmp_addr.line_no = line_no;
    tmp_addr.return_register = return_register;

    code += tmp_addr.getCode(mgr, length);

    code += "LOAD " + return_register->name + " " + return_register->name + "\n";
    length++;

    return code;
}

std::string LessThan::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    assignClobbers(mgr);

    value1->return_register = return_register;
    value2->return_register = clobber_reg[0];

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    code += "INC " + return_register->name + "\n"; length++;
    code += "SUB " + return_register->name + " " + clobber_reg[0]->name + "\n"; length++;

    freeClobbers(mgr);
    return code;
}

std::string GreaterThan::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    assignClobbers(mgr);

    value1->return_register = clobber_reg[0];
    value2->return_register = return_register;

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    code += "INC " + return_register->name + "\n"; length++;
    code += "SUB " + return_register->name + " " + clobber_reg[0]->name + "\n"; length++;

    freeClobbers(mgr);
    return code;
}

std::string LessThanOrEqualTo::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    assignClobbers(mgr);

    value1->return_register = return_register;
    value2->return_register = clobber_reg[0];

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    code += "SUB " + return_register->name + " " + clobber_reg[0]->name + "\n";
    length++;
    
    freeClobbers(mgr);
    return code;
}

std::string GreaterThanOrEqualTo::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    assignClobbers(mgr);

    value1->return_register = clobber_reg[0];
    value2->return_register = return_register;

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    code += "SUB " + return_register->name + " " + clobber_reg[0]->name + "\n";
    length++;
    
    freeClobbers(mgr);
    return code;
}

std::string EqualTo::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    assignClobbers(mgr);

    value1->return_register = return_register;
    value2->return_register = clobber_reg[0];

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    code += "RESET " + clobber_reg[1]->name + "\n"; length++;
    code += "ADD " + clobber_reg[1]->name + " " + return_register->name + "\n"; length++;
    code += "SUB " + return_register->name + " " + clobber_reg[0]->name + "\n"; length++;
    code += "JZERO " + return_register->name + "2" + "\n"; length++;
    code += "JUMP 5\n"; length++;
    code += "SUB " + clobber_reg[0]->name + " " + clobber_reg[1]->name + "\n"; length++;
    code += "JZERO " + clobber_reg[0]->name + "3" + "\n"; length++;
    code += "RESET " + return_register->name + "\n"; length++;
    code += "INC " + return_register->name + "\n"; length++;

    freeClobbers(mgr);
    return code;
}

std::string NotEqualTo::getCode(MemoryManager& mgr, ull& length)
{
    std::string code; 
    assignClobbers(mgr);

    value1->return_register = return_register;
    value2->return_register = clobber_reg[0];

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    code += "RESET " + clobber_reg[1]->name + "\n"; length++;
    code += "ADD " + clobber_reg[1]->name + " " + return_register->name + "\n"; length++;
    code += "SUB " + return_register->name + " " + clobber_reg[0]->name + "\n"; length++;
    code += "JZERO " + return_register->name + "2" + "\n"; length++;
    code += "JUMP 6\n"; length++;
    code += "SUB " + clobber_reg[0]->name + " " + clobber_reg[1]->name + "\n"; length++;
    code += "JZERO " + clobber_reg[0]->name + "2" + "\n"; length++;
    code += "JUMP 3\n"; length++;
    code += "INC " + return_register->name + "\n"; length++;
    code += "JUMP 2\n"; length++;
    code += "RESET " + return_register->name + "\n"; length++;

    freeClobbers(mgr);
    return code;
}

std::string ValueReference::getCode(MemoryManager& mgr, ull& length)
{
    value1->return_register = return_register;
    return value1->getCode(mgr, length);
}

std::string Addition::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    assignClobbers(mgr);

    value1->return_register = return_register;
    value2->return_register = clobber_reg[0];

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    code += "ADD " + return_register->name + " " + clobber_reg[0]->name + "\n";
    length++;

    freeClobbers(mgr);
    return code;
}

std::string Subtraction::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    assignClobbers(mgr);

    value1->return_register = return_register;
    value2->return_register = clobber_reg[0];

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    code += "SUB " + return_register->name + " " + clobber_reg[0]->name + "\n";
    length++;

    freeClobbers(mgr);
    return code;
}

std::string Multiplication::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    assignClobbers(mgr);

    value1->return_register = clobber_reg[0];
    value2->return_register = clobber_reg[1];

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    // Swap variables - greater should be first
    code += "RESET " + return_register->name + "\n"; length++;
    code += "ADD " + return_register->name + " " + clobber_reg[1]->name + "\n"; length++;
    code += "SUB " + return_register->name + " " + clobber_reg[0]->name + "\n"; length++;
    code += "JZERO " + clobber_reg[1]->name + " " + "7" + "\n"; length++;
    code += "RESET " + return_register->name + "\n"; length++;
    code += "ADD " + return_register->name + " " + clobber_reg[1]->name + "\n"; length++;
    code += "RESET " + clobber_reg[1]->name + "\n"; length++;
    code += "ADD " + clobber_reg[1]->name + " " + clobber_reg[0]->name + "\n"; length++;
    code += "RESET " + clobber_reg[0]->name + "\n"; length++;
    code += "ADD " + clobber_reg[0]->name + " " + return_register->name + "\n"; length++;
    
    // Multiply
    code += "RESET " + return_register->name + "\n"; length++;
    code += "JZERO " + clobber_reg[1]->name + " " + "7" + "\n"; length++;
    code += "JODD " + clobber_reg[1]->name + " " + "2" + "\n"; length++;
    code += "JUMP 2\n"; length++;
    code += "ADD " + return_register->name + " " + clobber_reg[0]->name + "\n"; length++;
    code += "SHL " + clobber_reg[0]->name + "\n"; length++;
    code += "SHR " + clobber_reg[1]->name + "\n"; length++;
    code += "JUMP -6\n"; length++;

    freeClobbers(mgr);
    return code;
}

std::string Division::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    assignClobbers(mgr);

    value1->return_register = clobber_reg[1];
    value2->return_register = clobber_reg[0];

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    // Check if val2 == 0 or if val1 < val2, if yes go to end (with ret_reg = 0)
    code += "RESET " + return_register->name + "\n"; length++;
    code += "JZERO " + clobber_reg[0]->name + " " + "30" + "\n"; length++;
    code += "ADD " + return_register->name + " " + clobber_reg[1]->name + "\n"; length++;
    code += "INC " + return_register->name + "\n"; length++;
    code += "SUB " + return_register->name + " " + clobber_reg[0]->name + "\n"; length++;
    code += "JZERO " + return_register->name + " " + "26" + "\n"; length++;

    code += "RESET " + return_register->name + "\n"; length++;
    code += "RESET " + clobber_reg[2]->name + "\n"; length++;
    code += "RESET " + clobber_reg[3]->name + "\n"; length++;
    code += "ADD " + clobber_reg[3]->name + " " + clobber_reg[1]->name + "\n"; length++;
    code += "JZERO " + clobber_reg[3]->name + " " + "5" + "\n"; length++;
    code += "SHR " + clobber_reg[3]->name + "\n"; length++;
    code += "INC " + clobber_reg[2]->name + "\n"; length++;
    code += "SHL " + clobber_reg[0]->name + "\n"; length++;
    code += "JUMP -4\n"; length++;

    code += "JZERO " + clobber_reg[2]->name + " " + "16" + "\n"; length++;
    code += "SHL " + clobber_reg[1]->name + "\n"; length++;

    code += "RESET " + clobber_reg[3]->name + "\n"; length++;
    code += "ADD " + clobber_reg[3]->name + " " + clobber_reg[1]->name + "\n"; length++;
    
    code += "INC " + clobber_reg[1]->name + "\n"; length++;
    code += "SUB " + clobber_reg[1]->name + " " + clobber_reg[0]->name + "\n"; length++;
    
    code += "JZERO " + clobber_reg[1]->name + " " + "2" + "\n"; length++;
    code += "JUMP 4\n"; length++;
    code += "SHL " + return_register->name + "\n"; length++;
    code += "ADD " + clobber_reg[1]->name + " " + clobber_reg[3]->name + "\n"; length++;
    code += "JUMP 4\n"; length++;
    code += "SHL " + return_register->name + "\n"; length++;
    code += "INC " + return_register->name + "\n"; length++;
    code += "DEC " + clobber_reg[1]->name + "\n"; length++;

    code += "DEC " + clobber_reg[2]->name + "\n"; length++;
    code += "JUMP -15\n"; length++;

    freeClobbers(mgr);
    return code;
}

std::string Modulo::getCode(MemoryManager& mgr, ull& length)
{
    std::string code; 
    assignClobbers(mgr);

    value1->return_register = return_register;
    value2->return_register = clobber_reg[1];

    code += value1->getCode(mgr, length);
    code += value2->getCode(mgr, length);

    // Check if val2 == 0 or if val1 < val2, if yes go to end (with ret_reg = 0 or ret_reg = val2)
    code += "JZERO " + clobber_reg[1]->name + " " + "2" + "\n"; length++;
    code += "JUMP 3\n"; length++;
    code += "RESET " + return_register->name + "\n"; length++;
    code += "JUMP 33\n"; length++;
    code += "RESET " + clobber_reg[0]->name + "\n"; length++;
    code += "ADD " + clobber_reg[0]->name + " " + return_register->name + "\n"; length++;
    code += "INC " + clobber_reg[0]->name + "\n"; length++;
    code += "SUB " + clobber_reg[0]->name + " " + clobber_reg[1]->name + "\n"; length++;
    code += "JZERO " + clobber_reg[0]->name + " " + "28" + "\n"; length++;

    code += "RESET " + clobber_reg[2]->name + "\n"; length++;
    code += "RESET " + clobber_reg[3]->name + "\n"; length++;
    code += "ADD " + clobber_reg[3]->name + " " + return_register->name + "\n"; length++;
    code += "JZERO " + clobber_reg[3]->name + " " + "5" + "\n"; length++;
    code += "SHR " + clobber_reg[3]->name + + "\n"; length++;
    code += "INC " + clobber_reg[2]->name + + "\n"; length++;
    code += "SHL " + clobber_reg[1]->name + + "\n"; length++;
    code += "JUMP -4\n"; length++;

    code += "RESET " + clobber_reg[0]->name + "\n"; length++;
    code += "ADD " + clobber_reg[0]->name + " " + clobber_reg[2]->name + "\n"; length++;

    code += "JZERO " + clobber_reg[2]->name + " " + "13" + "\n"; length++;
    code += "SHL " + return_register->name + "\n"; length++;

    code += "RESET " + clobber_reg[3]->name + "\n"; length++;
    code += "ADD " + clobber_reg[3]->name + " " + return_register->name + "\n"; length++;
    
    code += "INC " + return_register->name + "\n"; length++;
    code += "SUB " + return_register->name + " " + clobber_reg[1]->name + "\n"; length++;
    
    code += "JZERO " + return_register->name + " " + "2" + "\n"; length++;
    code += "JUMP 3\n"; length++;
    code += "ADD " + return_register->name + " " + clobber_reg[3]->name + "\n"; length++;
    code += "JUMP 2\n"; length++;
    code += "DEC " + return_register->name + "\n"; length++;
    
    code += "DEC " + clobber_reg[2]->name + "\n"; length++;
    code += "JUMP -12\n"; length++;

    code += "JZERO " + clobber_reg[0]->name + " " + "4" + "\n"; length++;
    code += "SHR " + return_register->name + "\n"; length++;
    code += "DEC " + clobber_reg[0]->name + "\n"; length++;
    code += "JUMP -3\n"; length++;

    freeClobbers(mgr);
    return code;
}

std::string Assignment::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    Register* B = mgr.assignRegister();

    expression->return_register = B;
    code += expression->getCode(mgr, length);

    Register* A = mgr.assignRegister();
    Address tmp_ident_addr(identifier);
    tmp_ident_addr.line_no = line_no;
    tmp_ident_addr.return_register = A;

    code += tmp_ident_addr.getCode(mgr, length);

    code += "STORE " + B->name + " " + A->name + "\n"; length++;

    if(identifier->var_type == Identifier::VT_VARIABLE)
    {
        if(mgr.isIterator(identifier->input_value.pid, this->line_no))
            throw TranslatorException("Iterator " + std::string(identifier->input_value.pid)
                + " cannot be modified!", this->line_no);
    
        mgr.markInitialized(identifier->input_value.pid, this->line_no);
    }

    mgr.freeRegister(A->name);
    mgr.freeRegister(B->name);
    
    if (stats)
        stats->commands_parsed++;

    return code;
}

std::string IfElse::getCode(MemoryManager& mgr, ull& length)
{
    std::string code, if_block, else_block;
    Register* A = mgr.assignRegister();

    condition->return_register = A;
    code += condition->getCode(mgr, length);
    mgr.freeRegister(A->name);

    ull block_len_if = 0;
    ull block_len_else = 0;

    for (vector<Command*>::iterator it = commands_if->begin(); it != commands_if->end(); it++)
    {
        if_block += (*it)->getCode(mgr, block_len_if);
    }

    for (vector<Command*>::iterator it = commands_else->begin(); it != commands_else->end(); it++)
    {
        else_block += (*it)->getCode(mgr, block_len_else);
    }

    code += "JZERO " + A->name + " " + to_string(block_len_else + 2) + "\n"; length++;
    code += else_block; length += block_len_else;
    code += "JUMP " + to_string(block_len_if + 1) + "\n"; length++;
    code += if_block; length += block_len_if;
    
    if (stats)
        stats->commands_parsed++;

    return code;
}

std::string If::getCode(MemoryManager& mgr, ull& length)
{
    std::string code, if_block;
    Register* A = mgr.assignRegister();

    condition->return_register = A;
    code += condition->getCode(mgr, length);
    mgr.freeRegister(A->name);

    ull block_len = 0;

    for (vector<Command*>::iterator it = commands_if->begin(); it != commands_if->end(); it++)
    {
        if_block += (*it)->getCode(mgr, block_len);
    }

    code += "JZERO " + A->name + " " + "2" + "\n"; length++;
    code += "JUMP " + to_string(block_len + 1) + " \n"; length++;
    code += if_block; length += block_len;

    if (stats)
        stats->commands_parsed++;

    return code;
}

std::string While::getCode(MemoryManager& mgr, ull& length)
{
    std::string code, if_block;
    Register* A = mgr.assignRegister();

    condition->return_register = A;

    ull cond_len = 0;
    code += condition->getCode(mgr, cond_len); length += cond_len;
    mgr.freeRegister(A->name);

    ull block_len = 0;

    for (vector<Command*>::iterator it = commands_if->begin(); it != commands_if->end(); it++)
    {
        if_block += (*it)->getCode(mgr, block_len);
    }

    code += "JZERO " + A->name + " " + "2" + "\n"; length++;
    code += "JUMP " + to_string(block_len + 2) + " \n"; length++;
    code += if_block; length += block_len;
    code += "JUMP " + to_string(-(long long)block_len -(long long)cond_len - 2) + "\n"; length++;

    if (stats)
        stats->commands_parsed++;

    return code;
}

std::string Repeat::getCode(MemoryManager& mgr, ull& length)
{
    std::string code, if_block, cond_block;
    Register* A = mgr.assignRegister();

    condition->return_register = A;

    ull cond_len = 0;
    cond_block += condition->getCode(mgr, cond_len);
    mgr.freeRegister(A->name);

    ull block_len = 0;

    for (vector<Command*>::iterator it = commands_if->begin(); it != commands_if->end(); it++)
    {
        if_block += (*it)->getCode(mgr, block_len);
    }

    code += if_block; length += block_len;
    code += cond_block; length += cond_len;
    code += "JZERO " + A->name + " " + "2" + "\n"; length++;
    code += "JUMP " + to_string(-(long long)cond_len -(long long)block_len - 1) + "\n"; length++;

    if (stats)
        stats->commands_parsed++;

    return code;
}

std::string ForTo::getCode(MemoryManager& mgr, ull& length)
{
    std::string code, cmds_block, cond_block, inc_block;

    mgr.declareInt(iter->input_value.pid, this->line_no);
    mgr.declareInt(to_var->input_value.pid, this->line_no);

    Assignment tmp_assign_init(iter, from_ref);
    tmp_assign_init.line_no = line_no;
    Assignment tmp_assign_to(to_var, to_ref);
    tmp_assign_to.line_no = line_no;

    code += tmp_assign_to.getCode(mgr, length);
    code += tmp_assign_init.getCode(mgr, length);

    mgr.markInitialized(iter->input_value.pid, this->line_no);
    mgr.markIterator(iter->input_value.pid, this->line_no);
    mgr.markInitialized(to_var->input_value.pid, this->line_no);
    
    Register* A = mgr.assignRegister();
    Register* B = mgr.assignRegister();

    to_cond->return_register = A;

    ull cond_len = 0;
    cond_block += to_cond->getCode(mgr, cond_len);

    ull inc_len = 0;
    Address tmp_it_address(iter);
    tmp_it_address.line_no = line_no;
    tmp_it_address.return_register = B;
    
    inc_block += tmp_it_address.getCode(mgr, inc_len);
    inc_block += "LOAD " + A->name + " " + B->name + "\n"; inc_len++;
    inc_block += "INC " + A->name + "\n"; inc_len++;
    inc_block += "STORE " + A->name + " " + B->name + "\n"; inc_len++;

    mgr.freeRegister(A->name);
    mgr.freeRegister(B->name);

    ull block_len = 0;

    for (vector<Command*>::iterator it = commands->begin(); it != commands->end(); it++)
    {
        cmds_block += (*it)->getCode(mgr, block_len);
    }

    code += cond_block; length += cond_len;
    code += "JZERO " + A->name + " " + "2" + "\n"; length++;
    code += "JUMP " + to_string(block_len + inc_len + 2) + " \n"; length++;
    code += cmds_block; length += block_len;
    code += inc_block; length += inc_len;
    code += "JUMP " + to_string(-(long long)block_len -(long long)cond_len
            -(long long)inc_len - 2) + "\n"; length++;

    mgr.free(iter->input_value.pid);
    mgr.free(to_var->input_value.pid);

    if (stats)
        stats->commands_parsed++;

    return code;
}

std::string ForDownTo::getCode(MemoryManager& mgr, ull& length)
{
    std::string code, cmds_block, cond_block, dec_block;

    mgr.declareInt(iter->input_value.pid, this->line_no);
    mgr.declareInt(to_var->input_value.pid, this->line_no);

    Assignment tmp_assign_init(iter, from_ref);
    tmp_assign_init.line_no = line_no;
    Assignment tmp_assign_to(to_var, to_ref);
    tmp_assign_to.line_no = line_no;

    code += tmp_assign_to.getCode(mgr, length);
    code += tmp_assign_init.getCode(mgr, length);
    
    mgr.markInitialized(iter->input_value.pid, this->line_no);
    mgr.markIterator(iter->input_value.pid, this->line_no);
    mgr.markInitialized(to_var->input_value.pid, this->line_no);

    Register* A = mgr.assignRegister();
    Register* B = mgr.assignRegister();
    
    to_cond->return_register = A;

    ull cond_len = 0;
    cond_block += to_cond->getCode(mgr, cond_len);

    ull dec_len = 0;
    Address tmp_it_address(iter);
    tmp_it_address.line_no = line_no;
    tmp_it_address.return_register = B;

    dec_block += tmp_it_address.getCode(mgr, dec_len);
    dec_block += "LOAD " + A->name + " " + B->name + "\n"; dec_len++;
    dec_block += "JZERO " + A->name + " " + "4" + "\n"; dec_len++;
    dec_block += "DEC " + A->name + "\n"; dec_len++;
    dec_block += "STORE " + A->name + " " + B->name + "\n"; dec_len++;

    mgr.freeRegister(A->name);
    mgr.freeRegister(B->name);

    ull block_len = 0;

    for (vector<Command*>::iterator it = commands->begin(); it != commands->end(); it++)
    {
        cmds_block += (*it)->getCode(mgr, block_len);
    }

    code += cond_block; length += cond_len;
    code += "JZERO " + A->name + " " + "2" + "\n"; length++;
    code += "JUMP " + to_string(block_len + dec_len + 2) + " \n"; length++;
    code += cmds_block; length += block_len;
    code += dec_block; length += dec_len;
    code += "JUMP " + to_string(-(long long)block_len -(long long)cond_len
            -(long long)dec_len - 2) + "\n"; length++;

    mgr.free(iter->input_value.pid);
    mgr.free(to_var->input_value.pid);

    if (stats)
        stats->commands_parsed++;

    return code;
}

std::string Read::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;
    Register* A = mgr.assignRegister();

    Address tmp_addr(identifier);
    tmp_addr.line_no = line_no;
    tmp_addr.return_register = A;

    code += tmp_addr.getCode(mgr, length);
    mgr.freeRegister(A->name);

    if (identifier->var_type == Identifier::VT_VARIABLE)
    {
        if(mgr.isIterator(identifier->input_value.pid, this->line_no))
            throw TranslatorException("Iterator " + std::string(identifier->input_value.pid)
                                      + " cannot be modified!", this->line_no);
    }

    code += "GET " + A->name + "\n"; length++;

    if(identifier->var_type == Identifier::VT_VARIABLE)
        mgr.markInitialized(identifier->input_value.pid, this->line_no);

    if (stats)
        stats->commands_parsed++;

    return code;
}

std::string Write::getCode(MemoryManager& mgr, ull& length)
{
    std::string code;

    if (value->type == Value::T_CONSTANT)
    {
        std::string num = to_string(value->input_value.number);
        const char* pid_cpy = new char[num.length()];
        strcpy((char*) pid_cpy, num.c_str());

        mgr.declareInt(pid_cpy, this->line_no);
        mgr.markInitialized(pid_cpy, this->line_no);

        Variable tmp_var(pid_cpy);
        tmp_var.line_no = line_no;
        Address tmp_addr(&tmp_var);
        tmp_addr.line_no = line_no;

        Register* A = mgr.assignRegister();
        Register* B = mgr.assignRegister();

        tmp_addr.return_register = A;
        value->return_register = B;

        code += tmp_addr.getCode(mgr, length);
        code += value->getCode(mgr, length);

        mgr.freeRegister(A->name);
        mgr.freeRegister(B->name);

        code += "STORE " + B->name + " " + A->name + "\n"; length++;
        code += "PUT " + A->name + "\n"; length++;

        mgr.free(pid_cpy);
    }

    else
    {
        if((static_cast<Identifier*> (value))->var_type == Identifier::VT_VARIABLE)
        {
            if(!mgr.isInitialized(value->input_value.pid, this->line_no))
                throw TranslatorException("Variable " + std::string(value->input_value.pid)
                                          + " is referred to before being initialized!", this->line_no);
        }

        Address tmp_addr(static_cast<Identifier*> (value));
        tmp_addr.line_no = line_no;
        Register* A = mgr.assignRegister();

        tmp_addr.return_register = A;

        code += tmp_addr.getCode(mgr, length);
        mgr.freeRegister(A->name);

        code += "PUT " + A->name + "\n"; length++;
    }

    if (stats)
        stats->commands_parsed++;
        
    return code;
}