%{
	#include <iostream>
    #include <cstdio>
    #include <cstdlib>
    #include <chrono>
    
    #include "Token.hpp"
    #include "Exceptions.hpp"
    #include "StatisticsContext.hpp"
    #include "Translator.hpp"
    
    #define YYDEBUG 1

	using namespace std;

	extern int yylex();
	void yyerror(const char *s);

    extern FILE* yyin;    
    extern int yylineno;

    StatisticsContext statistics;
    Translator translator(statistics, false);

    // Time of program execution:
    std::chrono::high_resolution_clock::time_point start;

    // Decorator function for counting lines
    Block* assignLine(Block* block, ull lineno)
    {
        block->line_no = lineno;
        return block;
    }

    // Extract line number from token
    ull lno(Token& token)
    {
        return token.line_no;
    }

    // Extract string data from token
    const char* sval(Token& token)
    {
        return token.data.sval;
    }

    // Extract integer data from token
    ull ival(Token& token)
    {
        return token.data.ival;
    }
%}

// Pass the include depenencies:
%code requires { 
    #include "Token.hpp"
    #include "Blocks.hpp"
    #include <vector>
}

// Declare an union of allowed token types:
%union {
    Token token;
    
    Identifier* bidentifier;
    Value* bvalue;
    Condition* bcondition;
    Expression* bexpression;
    Command* bcommand;
    std::vector<Command*>* vcommands;
}

%locations

// Token definitions:
%token NUM
%token PIDENTIFIER

%token BEGIN_
%token DECLARE
%token DO
%token DOWNTO
%token ELSE
%token END
%token ENDFOR
%token ENDIF
%token ENDWHILE
%token FOR
%token FROM
%token IF
%token READ
%token REPEAT
%token THEN
%token TO
%token UNTIL
%token WHILE
%token WRITE

// Token typesD:
%type<token> NUM
%type<token> PIDENTIFIER
%type<token> '<' '>' '=' '!' ':' '+' '-' '*' '/' '%'

%type<token> BEGIN_
%type<token> DECLARE
%type<token> DO
%type<token> DOWNTO
%type<token> ELSE
%type<token> END
%type<token> ENDFOR
%type<token> ENDIF
%type<token> ENDWHILE
%type<token> FOR
%type<token> FROM
%type<token> IF
%type<token> READ
%type<token> REPEAT
%type<token> THEN
%type<token> TO
%type<token> UNTIL
%type<token> WHILE
%type<token> WRITE

%type<bidentifier> identifier
%type<bvalue> value
%type<bcondition> condition
%type<bexpression> expression
%type<bcommand> command
%type<vcommands> commands


%%
program
: DECLARE declarations BEGIN_ commands END              {
                                                            try{ translator.translate($4); }
                                                            catch(Exception& exc)
                                                            {
                                                                statistics.stopMonitoring();
                                                                cerr << exc.what() << endl;
                                                                YYABORT;
                                                            }
                                                        }
| BEGIN_ commands END                                   {
                                                            try{ translator.translate($2); }
                                                            catch(Exception& exc)
                                                            {
                                                                statistics.stopMonitoring();
                                                                cerr << exc.what() << endl;
                                                                YYABORT;
                                                            }
                                                        }
;

declarations
: declarations ',' PIDENTIFIER                          {
                                                            try{ translator.declareInt(sval($3), lno($3)); }
                                                            catch(Exception& exc)
                                                            {
                                                                cerr << exc.what() << endl;
                                                                YYABORT;
                                                            }
                                                        }
| declarations ',' PIDENTIFIER '(' NUM ':' NUM ')'      {
                                                            try{ translator.declareArray(sval($3), ival($5), ival($7), lno($3)); }
                                                            catch(Exception& exc)
                                                            {
                                                                cerr << exc.what() << endl;
                                                                YYABORT;
                                                            }
                                                        }
| PIDENTIFIER                                           {
                                                            try{ translator.declareInt(sval($1), lno($1)); }
                                                            catch(Exception& exc)
                                                            {
                                                                cerr << exc.what() << endl;
                                                                YYABORT;
                                                            }
                                                        }
| PIDENTIFIER '(' NUM ':' NUM ')'                       {
                                                            try{ translator.declareArray(sval($1), ival($3), ival($5), lno($1)); }
                                                            catch(Exception& exc)
                                                            {
                                                                cerr << exc.what() << endl;
                                                                YYABORT;
                                                            }
                                                        }
;

commands
: commands command          { $1->push_back($2); $$ = $1; }
| command                   {
                                vector<Command*>* vect = new vector<Command*>();
                                vect->push_back($1);
                                $$ = vect;
                            }
;

command
: identifier ':' '=' expression ';'                                 { $$ = (Command*) assignLine(new Assignment($1, $4, &statistics), lno($2)); }
| IF condition THEN commands ELSE commands ENDIF                    { $$ = (Command*) assignLine(new IfElse($2, $4, $6, &statistics), lno($1)); }
| IF condition THEN commands ENDIF                                  { $$ = (Command*) assignLine(new If($2, $4, &statistics), lno($1)); }
| WHILE condition DO commands ENDWHILE                              { $$ = (Command*) assignLine(new While($2, $4, &statistics), lno($1)); }
| REPEAT commands UNTIL condition ';'                               { $$ = (Command*) assignLine(new Repeat($4, $2, &statistics), lno($1)); }
| FOR PIDENTIFIER FROM value TO value DO commands ENDFOR            { $$ = (Command*) assignLine(new ForTo(sval($2), $4, $6, $8, &statistics), lno($1)); }
| FOR PIDENTIFIER FROM value DOWNTO value DO commands ENDFOR        { $$ = (Command*) assignLine(new ForDownTo(sval($2), $4, $6, $8, &statistics), lno($1)); }
| READ identifier ';'                                               { $$ = (Command*) assignLine(new Read($2, &statistics), lno($1)); }
| WRITE value ';'                                                   { $$ = (Command*) assignLine(new Write($2, &statistics), lno($1)); }
;

expression
: value                 { $$ = (Expression*) assignLine(new ValueReference($1), $1->line_no); }
| value '+' value       { $$ = (Expression*) assignLine(new Addition($1, $3), lno($2)); }
| value '-' value       { $$ = (Expression*) assignLine(new Subtraction($1, $3), lno($2)); }
| value '*' value       { $$ = (Expression*) assignLine(new Multiplication($1, $3), lno($2)); }
| value '/' value       { $$ = (Expression*) assignLine(new Division($1, $3), lno($2)); }
| value '%' value       { $$ = (Expression*) assignLine(new Modulo($1, $3), lno($2)); }
;

condition
: value '=' value       { $$ = (Condition*) assignLine(new EqualTo($1, $3), lno($2)); }
| value '!' '=' value   { $$ = (Condition*) assignLine(new NotEqualTo($1, $4), lno($2)); }
| value '<' value       { $$ = (Condition*) assignLine(new LessThan($1, $3), lno($2)); }
| value '>' value       { $$ = (Condition*) assignLine(new GreaterThan($1, $3), lno($2)); }
| value '<' '=' value   { $$ = (Condition*) assignLine(new LessThanOrEqualTo($1, $4), lno($2)); }
| value '>' '=' value   { $$ = (Condition*) assignLine(new GreaterThanOrEqualTo($1, $4), lno($2)); }
;

value
: NUM           { $$ = (Value*) assignLine(new Constant(ival($1)), lno($1)); }
| identifier    { $$ = $1; }
;

identifier
: PIDENTIFIER                       { $$ = (Identifier*) assignLine(new Variable(sval($1)), lno($1)); }
| PIDENTIFIER '(' PIDENTIFIER ')'   { $$ = (Identifier*) assignLine(new ArrayFieldVarIndexed(sval($1), sval($3)), lno($1)); }
| PIDENTIFIER '(' NUM ')'           { $$ = (Identifier*) assignLine(new ArrayFieldNumIndexed(sval($1), ival($3)), lno($1)); }
;


%%

void yyerror(const char *s)
{
    fprintf(stderr, "Parser error at line %d: %s\n", yylineno, s);
}

void at_exit()
{
    auto stop = std::chrono::high_resolution_clock::now();
    std::chrono::duration<double> elapsed = stop - start;

    cout << "Program execution took " << elapsed.count() << " seconds." << endl;
}

int main(int argc, char** argv)
{
    // Start measuring execution time:
    start = std::chrono::high_resolution_clock::now();
    // Program's duration will be calculated and printed at exit:
    std::atexit(at_exit);

    // Switch yydebug to 1 to turn on the parser debug mode: 
	yydebug = 0;

    // Ignore the first parameter (program path):
    argc--;
    argv++;

    if(argc != 2)
    {
        cout << "Inappropriate number of parameters!" << endl;
        cout << "Usage: " << endl;
        cout << "kompilator <input file name> <output file name>" << endl;
        
        return 1;
    }

    FILE* in_file = fopen(argv[0], "r");
    
    if (!in_file)
    {
        cout << "Failed to open file: " << argv[0] << endl;
        return 2;
    }

    yyin = in_file;

    if (yyparse() != 0)
    {
        fclose(in_file);
        cout << "Compilation failed!" << endl;
        return 3;    
    }

    fclose(in_file);

    if (!translator.saveToFile(argv[1]))
        return 2;
    	
    return 0;
}
