CC = g++

all: compiler

gram.tab.c gram.tab.h: gram.y
	bison --debug -d -t -v gram.y

lex.yy.c: lexer.l
	flex lexer.l

compiler: gram.tab.c gram.tab.h lex.yy.c
	$(CC) -std=c++11 gram.tab.c lex.yy.c Translator.cpp \
	BlocksDefs.cpp BlocksCode.cpp MemoryManager.cpp \
	Exceptions.cpp StatisticsContext.cpp \
	-lfl -lpthread -o kompilator

clean:
	rm -f kompilator gram.tab.c lex.yy.c gram.tab.h gram.output