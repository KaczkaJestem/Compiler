#include "Exceptions.hpp"

Exception::Exception(std::string msg, ull line_no)
    : _msg(msg), _line_no(line_no)
{
    if (line_no > 0)
        _msg = "Line " + std::to_string(_line_no) + ": " + _msg;
}

TranslatorException::TranslatorException(std::string msg, ull line_no)
    : Exception("Translator exception: " + msg, line_no)
{
}

MemoryException::MemoryException(std::string msg, ull line_no)
    : Exception("Memory exception: " + msg, line_no)
{
}

const char* TranslatorException::what() const noexcept
{
    return _msg.c_str();
}

const char* MemoryException::what() const noexcept
{
    return _msg.c_str();
}
